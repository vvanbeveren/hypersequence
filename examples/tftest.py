import numpy as np
import tensorflow.keras as tfk
import tempfile
import pathlib
import hypersequence as hs

# Map 10x10 random array to 3x3x3 cube
# ------------------------------------

def make_dataset(test_file):
    # Create non-sense BATCH_SIZE
    with hs.HyperSequenceWriter(test_file, 32) as hsw:
        for i in range(1000):
            inp = np.random.random((20, 20))
            out = np.random.random((3, 3, 3))
            hsw.append(inputs=inp, outputs=out)


def make_model(inp_shape: tuple, outp_shape: tuple) -> tfk.Model:
    model = tfk.models.Sequential(
        [
            tfk.layers.InputLayer(input_shape=inp_shape),
            tfk.layers.Flatten(),
            tfk.layers.Dense(np.prod(outp_shape), activation='relu'),
            tfk.layers.Reshape(outp_shape)
        ]
    )
    model.compile(optimizer='adam', loss='mean_squared_error')
    return model


def train_model(hsq: hs.HyperSequence, model : tfk.Model):
    fseq_b64 = hsq.rebatch(64)
    train, validation = fseq_b64.split(0.8)

    model.fit(
        train.as_keras(shuffle_on_epoch=True),
        epochs=16,
        validation_data=validation.as_keras())



if __name__ == "__main__":

    with tempfile.TemporaryDirectory() as tempdir:
        hs_file = pathlib.Path(tempdir) / "temp.hsq"

        make_dataset(hs_file)

        with hs.HyperSequenceFile(hs_file) as hsq:

            (inp_dtype,), (out_dtype, ) = hsq.dtypes()

            model = make_model(inp_dtype.shape, out_dtype.shape)

            train_model(hsq, model)


