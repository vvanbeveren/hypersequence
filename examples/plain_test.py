import hypersequence as hs
import tempfile
import pathlib as pl
import numpy as np

if __name__ == "__main__":
    with tempfile.TemporaryDirectory() as temp_dir:

        test_file = pl.Path(temp_dir) / "test.hsq"

        write_data = []
        read_back = []
        batch_size = 12

        with hs.HyperSequenceWriter(test_file, batch_size) as hsw:
            for i in range(3600):
                inp1 = np.random.random((5, 5))
                inp2 = np.random.random(4)
                out1 = np.random.random((2, 10, 2))
                hsw.append(inputs=(inp1, inp2), outputs=out1)
                write_data.append((inp1, inp2, out1))


        with hs.HyperSequenceFile(test_file) as hsq:

            for i in range(len(hsq)):
                ((inp1, inp2), out1) = hsq[i]
                for j in range(batch_size):
                    read_back.append((inp1[j].copy(),inp2[j].copy(),out1[j].copy()))


        for i, (w, r) in enumerate(zip(write_data, read_back)):
            for j in range(3):
                assert((w[j] == r[j]).all())


        # rebatch test
        with hs.HyperSequenceFile(test_file) as hsq:
            fs2 = hs.rebatch(hsq, batch_size * 3)
            for i in range(len(fs2)):
                ((inp1, inp2), out1) = fs2[i]
                assert(inp1.shape[0] == batch_size * 3)

            assert(len(fs2) == len(hsq) // 3)

            fs3 = hs.rebatch(hsq, batch_size // 3)
            for i in range(len(fs3)):
                ((inp1, inp2), out1) = fs3[i]
                assert(inp1.shape[0] == batch_size // 3)

            assert(len(fs3) == len(hsq) * 3)


        with hs.HyperSequenceFile(test_file, use_mmap=False) as hsq:
            fs2 = hs.rebatch(hsq, batch_size * 3)
            for i in range(len(fs2)):
                ((inp1, inp2), out1) = fs2[i]
                assert(inp1.shape[0] == batch_size * 3)

            assert(len(fs2) == len(hsq) // 3)

            fs3 = hs.rebatch(hsq, batch_size // 3)
            for i in range(len(fs3)):
                ((inp1, inp2), out1) = fs3[i]
                assert(inp1.shape[0] == batch_size // 3)

            assert(len(fs3) == len(hsq) * 3)

        with hs.HyperSequenceFile(test_file, use_mmap=False, thread_safe=False) as hsq:
            fs2 = hs.rebatch(hsq, batch_size * 3)
            for i in range(len(fs2)):
                ((inp1, inp2), out1) = fs2[i]
                assert(inp1.shape[0] == batch_size * 3)

            assert(len(fs2) == len(hsq) // 3)

            fs3 = hs.rebatch(hsq, batch_size // 3)
            for i in range(len(fs3)):
                ((inp1, inp2), out1) = fs3[i]
                assert(inp1.shape[0] == batch_size // 3)

            assert(len(fs3) == len(hsq) * 3)

        with hs.HyperSequenceFile(test_file, copy_buffer=True, thread_safe=False) as hsq:
            fs2 = hs.rebatch(hsq, batch_size * 3)
            for i in range(len(fs2)):
                ((inp1, inp2), out1) = fs2[i]
                assert(inp1.shape[0] == batch_size * 3)

            assert(len(fs2) == len(hsq) // 3)

            fs3 = hs.rebatch(hsq, batch_size // 3)
            for i in range(len(fs3)):
                ((inp1, inp2), out1) = fs3[i]
                assert(inp1.shape[0] == batch_size // 3)

            assert(len(fs3) == len(hsq) * 3)
            x = hsq[0]

    # This causes an exception if copy_buffer=False, due to backing buffer being closed.
    print(len(x))
